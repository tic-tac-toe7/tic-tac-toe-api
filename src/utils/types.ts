import { SquareStateEnum } from './enums';

export type MatrixType = Array<Array<SquareStateEnum | null>>;
export interface StartGameInterface {
  start: boolean;
  symbol: SquareStateEnum;
}
