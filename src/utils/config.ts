export const PORT = process.env.PORT || '9000';
export const CORS_ORIGIN = process.env.CORS_ORIGIN || 'http://localhost:3000';
export const NODE_ENV = process.env.NODE_ENV || 'development';
