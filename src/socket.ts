import { Server as SocketServer, Socket } from 'socket.io';
import { NODE_ENV, PORT } from '@utils/config';
import { Server } from 'http';
import logger from './utils/logger';
import { MatrixType } from './utils/types';
import { SquareStateEnum, WinningStatus } from './utils/enums';

let connection: SocketClass | null = null;

class SocketClass {
  private io: SocketServer;

  connect(server: Server) {
    this.io = new SocketServer(server, {
      serveClient: false,
      cors: {
        origin: '*'
      }
    });

    this.io.on('connection', async (socket: Socket) => {
      logger.info(`connection`);
      if (NODE_ENV === 'development') {
        logger.info(`Socket client connected on port ${PORT} with ID ${socket.id}`);
      }

      socket.on('join_game', async (message: { roomId: string }) => {
        console.log('New User Joining room: ', message);

        const connectedSockets = this.io.sockets.adapter.rooms.get(message.roomId);
        const socketRooms = Array.from(socket.rooms.values()).filter((r) => r !== socket.id);

        if (socketRooms.length > 0 || (connectedSockets && connectedSockets.size === 2)) {
          socket.emit('room_join_error', {
            error: 'Room is full please choose another room to play'
          });
        } else {
          await socket.join(message.roomId);
          socket.emit('room_joined');
        }
      });

      socket.on('start_game', () => {
        const gameRoom = this.getSocketGameRoom(socket);
        if (this.io.sockets.adapter.rooms.get(gameRoom)?.size === 2) {
          socket.emit('on_start_game', {
            start: true,
            symbol: SquareStateEnum.Cross
          });
          socket.to(gameRoom).emit('on_start_game', {
            start: false,
            symbol: SquareStateEnum.Circle
          });
        }
      });

      socket.on('update_game', (message: MatrixType) => {
        const gameRoom = this.getSocketGameRoom(socket);
        socket.to(gameRoom).emit('on_game_update', message);
      });

      socket.on(
        'game_win',
        (message: { currentPlayer: number; otherPlayer: number; status: WinningStatus }) => {
          const gameRoom = this.getSocketGameRoom(socket);
          let currentPlayerStatus = WinningStatus.Tie;
          let otherPlayerStatus = WinningStatus.Tie;
          if (message.status === WinningStatus.Won) {
            currentPlayerStatus = WinningStatus.Won;
            otherPlayerStatus = WinningStatus.Lost;
          } else {
            currentPlayerStatus = WinningStatus.Lost;
            otherPlayerStatus = WinningStatus.Won;
          }
          socket.emit('on_game_win', {
            currentPlayer: message.currentPlayer,
            otherPlayer: message.otherPlayer,
            status: currentPlayerStatus
          });
          socket.to(gameRoom).emit('on_game_win', {
            currentPlayer: message.otherPlayer,
            otherPlayer: message.currentPlayer,
            status: otherPlayerStatus
          });
        }
      );

      socket.on('disconnect', () => {
        if (NODE_ENV === 'development') {
          logger.info(`Socket client disconnected. Socket ID: ${socket.id}`);
        }
      });
    });
  }

  private getSocketGameRoom(socket: Socket): string {
    const socketRooms = Array.from(socket.rooms.values()).filter((r) => r !== socket.id);
    const gameRoom = socketRooms && socketRooms[0];
    return gameRoom;
  }

  static init(server: Server) {
    if (!connection) {
      connection = new SocketClass();
      connection.connect(server);
    }
  }
}

export default {
  connect: SocketClass.init
};
