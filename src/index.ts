import 'dotenv/config';
import express from 'express';
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import { CORS_ORIGIN, PORT } from '@utils/config';
import cors from 'cors';
import logger from './utils/logger';
import SocketConnection from './socket';

const app = express();
app.use(express.json());
app.use(
  cors({
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    origin: CORS_ORIGIN,
    credentials: true,
    optionsSuccessStatus: 200,
    preflightContinue: false,
    allowedHeaders: [
      'Origin',
      'X-Requested-With',
      'Content-Type',
      'Accept',
      'auth',
      'Cookies',
      'set-cookie'
    ]
  })
);
app.use(
  helmet({
    crossOriginResourcePolicy: false
  })
);
app.use(cookieParser());

(async () => {
  try {
    const server = app.listen(PORT, () => {
      logger.info(`Server listening at http://localhost:${PORT}`);

      SocketConnection.connect(server);
    });
  } catch (e) {
    logger.error('Failed to connect to databae, Goodbye', e);
    process.exit(1);
  }
})();
