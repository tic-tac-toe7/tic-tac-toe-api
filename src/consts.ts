export enum SocketEventsType {
  USER_CONNECTED = 'userConnected',
  USER_DISCONNECTED = 'userDisconnected',
  STATUS = 'status',
  TEST = 'test',
  DROPIN_FINISH_CRON = 'dropInFinishCron',
  USER_CREDIT_HOURS_UPDATED = 'userCreditHoursUpdated',
  USER_SUBSCRIPTION_UPDATED = 'userSubscriptionUpdated',
  USER_SUBSCRIPTION_CANCELED = 'userSubscriptionCanceled',
  USER_INVOICE_UPDATED = 'userInvoiceUpdated',
  PASSWORD_CHANGED = 'passwordChanged',
  USER_DATA_UPDATED = 'userDataUpdated'
}
